
# Usage:

```
// test.js
require('ansi-escape-string');

console.log( 'red text'.red() );
console.log( 'green text'.green() );
```

```
$ node test.js
// red text
// green text

$ node test.js > outputFile
$ cat outputFile
// red text
// green text


// This modules useful feature!!
$ cat -v outputFile
// ^[[31mred text^[[39m
// ^[[32mgreen text^[[39m

# desc

Focus above last example!
[colors](https://www.npmjs.com/package/colors) doesn't support ansi escaped string! It only print test to color.
You can not save color from `node colorsExample.js > outputFile`

[clor](https://www.npmjs.com/package/clor) support ansi escaped string.
But doesn't have convenient API like a `console.log( 'Some red text'.red() )`
