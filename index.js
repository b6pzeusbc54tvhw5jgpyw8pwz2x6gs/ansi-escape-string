const ansiStyles = require('ansi-styles');

var key, color;
for( key in ansiStyles.color ) {

	val = ansiStyles.color[ key ];
	if( typeof val !== 'object' ) continue;
	if( ! val.open || ! val.close ) continue;

	String.prototype[ key ] = (function() {

		var color = val;
		return function() {
			var str = this.toString();
			return color.open + str + color.close;
		};

	}());
}

//test
//console.log( 'red text'.red() );
//console.log( 'green text'.green() );
